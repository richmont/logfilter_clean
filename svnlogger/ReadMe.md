# README #

## Anleitung für den integrierten SVN-Client Prototype 
### Inhalt
1. Installieren von VisualSVN und SVN
2. Erstellen eines Repositorries "teamprojekt"
3. Erstellen der "protocol.txt"
4. Anpassen des Sourcecodes für den SVN Server

## 1. Installieren von SVNVisual und SVN-Client

Für VisualSVN haben wir uns wegen der einfachen Einrichtung und Bediengung entschiedern. Zudem wurde, zu Testzwecken, der SVN-Server nur im Lokalem Netzwerk benutzt.

Download VisualSVN from [http://www.visualsvn.com/](https://www.visualsvn.com/server/download/2.7/). Wir benutzten die Version 2.7.8.

Getting started : [http://www.visualsvn.com/server/getting-started/](http://www.visualsvn.com/server/getting-started/)


Den SVN_Client benutzten wir in der Version 1.7.18 für Windows. Zu finden als MSI-Installer oder als Zip unter diesen Link: [http://sourceforge.net/projects/win32svn/files/1.7.18/apache22/](http://sourceforge.net/projects/win32svn/files/1.7.18/apache22/)

Es sollte auch möglich sein eine neuere Version benutzen zu können. Allerdings benutzen wir die SVN-API-Version 1.7.17. SVN ist zwar sehr gut abwärts kompatibel, aber man kann ja auf Nummer sicher gehen.

Nach der Installation sicherstellen, das die System-Umgebungsvariable für svn.exe gesetzt wurde. Prüfen kann man das in dem man sich in der Konsole den Befehl "path" eingibt und prüft ob ein Pfad, ähnlich wie der folgende, ausgegeben wurde: "C:\Programme\Apache-Subversion-1.8.9\bin".

## 2. Erstellen eines Repositories in VisualSVN

SVN Vsiual  Server Manager starten.

Als ersten Schritt https auschalten, mit rechts klick auf "VisualSVN", dann "Properties", im Reiter Network den Hacken bei "Use secure connection" entfernen. Der Prototyp benutzt derzeit nur unverschlüsselte http Verbindungen.
Zusätzlichen noch den gewünschten Port auswählen.Wir benutzten Port 8080.

Unter Reiter "Storage" kann noch ausgewählt werden wo das Root Verzeichnis für die Repositories des Servers sein sollen. Danach das Propperties Fenster mit OK bestätigen und verlassen.

Jetzt können wir das Repositorie anlegen, dazu rechts klick auf "Repositories". Im Kontextmenü dann auf "Create new Repository"

Im Wizard Namen vergeben, auf der nächsten Seite "empty repository" auswählen.
Für die Zugriffsrechte zunächst "Nobody has Access" auswählen.

Dann müssen wir benutzer Anlegen dazu rechts click auf "Users" und dann "Create User...". Namen und PW vergeben.

Den neuen Benutzer dann das Recht gewähren auf das Repositorie zuzugreifen, mit rechts klick auf das Repositorie, dann auf "Properties...", im Reiter Security auf "Add" klicken und den neuen Benutzer auswählen.

## 3. Erstellen der "Protocol.txt"

Dazu benötigen wir lediglich ein Reihe von Konsolen-Befehlen. Wechslen Sie mit der Konsole in ein Beliebiges Verzeichnis, wo Sie temporär eine Arbeitskopie des Repositories anlegen.


    D:\>cd temp_svn
Als nächsten Schrit führen wir den Checkout aus, um die Arbeitskopie des Repositories zu erzeugen.

	D:\temp_svn>svn checkout http://laptop:8080/svn/test1/ --username test --password test
	Checked out revision 0.
Sollte es zu einem Fehler kommen prüfen Sie nochmal Benutzername und Passwort.
	
Mit `cd` in das Arbeits-Verzeichnis des Repositpries wechseln. Mit `dir /A` können wir uns noch mal Vergewissern das wir in einem SVN Verzeichnis sind, dies sieht man an dem versteckten Ordner `.svn`.

	D:\temp_svn>cd test1
	D:\temp_svn\test1>dir /A
 		Datenträger in Laufwerk D: ist Daten
 		Volumeseriennummer: C211-B397

 	Verzeichnis von D:\temp_svn\test1

	09.11.2014  13:43    <DIR>          .
	09.11.2014  13:43    <DIR>          ..
	09.11.2014  13:43    <DIR>          .svn
		0 Datei(en),              0 Bytes
        3 Verzeichnis(se), 24.278.855.680 Bytes frei
Nun erstellen wir die Protokoll Datei, mit der unser integrierter SVN-Client arbeiten soll. Der Name, der Protokoll Datei wurde hier frei gewählt. Sollte ein anderer Name verwendet werden, muss dieser im Source-Code des SVN-Clients ebenfalls geändert werden. Zum Erzeugen einer leeren Text Datei benutzen man den folgenden Befehl:

	D:\temp_svn\test1>type nul >protocol.txt

Nun muss die Datei noch committet werden. Dazu dienen die nächsten Befehle:

	D:\temp_svn\test1>svn status
	?       protocol.txt
	
	D:\temp_svn\test1>svn add protocol.txt
	A         protocol.txt
	
	D:\temp_svn\test1>svn commit protocol.txt -m "Initial commit, create file protocol.txt"
	Adding         protocol.txt
	Transmitting file data .
	Committed revision 1.

Nun kann der integrierte SVN-Client auf die Protokoll-Datei zugreifen bzw. mit ihr arbeiten. Das für die Erstellung der Datei angelegte Verzeichnis kann nun wieder gelöscht werden.
	
	D:\>rmdir -S temp_svn

## 4. Anpassen des Sourcecodes für den SVN-Server

Dazu muss der Setup Teil im Source Code bearbeitet werden. Es müssen Benutzername, Passwort, URL, Name der Protokol-Datei und SVN-Befehl angepasst werden. Wichtig ist dabei zu beachten das am Ende des URL-Pfades kein Slash steht. Sonst kann der Client den Pfad nicht richtig interpretieren. Ebenso der Pfad zur Protokoll-Datei im SVN-Befehl, dieser setzt sich zusammen aus Lokalem Repositorie Pfad plus Name der Protokoll-Datei.

	/*SETUP*/
	/*===========================================================================*/
	
	static const char *USERNAME = "test";
	static const char *PASSWORD = "test";
	
	// URL to connect the SVN Server:
	static const char *URL = "http://laptop:8080/svn/test1";
	
	// Client name:
	static const char *CLIENTNAME = "Filtertreiber";

	//Working path:
	static const char *WCPATH = "C:/temp/svn/";

	// File for the Record:
	static const char *PROTOCOL_FILE = "protocol.txt";
	// Command to do the commit:
	static const char *COMMIT_CMD = "svn commit C:/temp/svn/test1/protocol.txt --message ";
		
	/*==========================================================================*/
