#include "svn_client.h"
#include "svn_cmdline.h"
#include "svn_pools.h"
#include "svn_config.h"
#include "svn_fs.h"
#include "svn_ra.h"
#include "svn_path.h"
#include "svn_repos.h"

#include "svnlogger.h"

/*================SETUP====================================================*/

static const char *USERNAME = "test";
static const char *PASSWORD = "test";

// URL to connect the SVN Server:
static const char *URL = "http://hugonb2:8080/svn/TPDebug";
//static const char *URL = "http://hugonb2:8080/svn/teamprojekt";

// Client name:
static const char *CLIENTNAME = "Filtertreiber";
//Working path:
static const char *WCPATH = "C:/temp/svn/";
// File for the Record:
static const char *PROTOCOL_FILE = "protocol.txt";
// Command to do the commit:
static const char *COMMIT_CMD = "svn commit C:/temp/svn/TPDebug/protocol.txt --message ";
//static const char *COMMIT_CMD = "svn commit C:/temp/svn/teamprojekt/protocol.txt --message ";

/*==========================================================================*/

int main(int argc, const char* argv[])
{
	const char *username = USERNAME;
	const char *password = PASSWORD;
	const char *url = URL;
	const char *wc_path = WCPATH;
	const char *file = PROTOCOL_FILE;
	const char *commit_cmd = COMMIT_CMD;
	
	/*Create a memory pool*/
	apr_pool_t *pool;
	apr_pool_t *subpool1;
	
	const char *config_dir = NULL;
	apr_hash_t *cfg_hash;

	svn_error_t *err;

	svn_revnum_t rev = 0;
	svn_client_ctx_t *ctx;
	svn_ra_session_t *ra_session = NULL;
	svn_auth_baton_t *ab = NULL;

	const char *target_dir;
	svn_revnum_t *result_rev = NULL;
	
	/*Setup the APR internal data structures*/
	if (svn_cmdline_init("my small svn client", NULL) != 0)
		return EXIT_FAILURE;
		

	/*Create top-level pool*/
	pool = svn_pool_create(NULL);

	// initialize remote access API
	svn_ra_initialize(pool);

	/*Create Client context*/
	err = svn_client_create_context(&ctx, pool);
	if (err) goto hit_error;
	ctx->client_name = CLIENTNAME;

	/*Check if config files exist*/
	err = svn_config_ensure(config_dir, pool);
	if (err) goto hit_error;

	err = svn_config_get_config(&cfg_hash, NULL, pool);
	if (err) goto hit_error;
	ctx->config = cfg_hash;
	
	/*Create Subpool1 of Pool*/
	subpool1 = svn_pool_create(pool);

	/*Store the auth_naton in ctx*/
	ab = createAuthenticationBaton(ab, username, password, config_dir, subpool1);
	ctx->auth_baton = ab;

	svn_ra_callbacks2_t *callbacks;
	/*Create RA Callbacks*/
	err = svn_ra_create_callbacks(&callbacks, pool);
	if (err) goto hit_error;
	callbacks->auth_baton = ab;
	
	/* Validate the REPOS_URL */
	if (!svn_path_is_url(url)){
		printf("does not appear to be a URL");
	};
	//TODO error handle
	err = svn_client_open_ra_session(&ra_session, url, ctx, subpool1);
	if (err) goto hit_error;
	
	err = svn_ra_open4(&ra_session, NULL, url, NULL, callbacks, NULL, cfg_hash, subpool1);
	
	fprintf(stdout, "connected to %s\n", url);
	err = svn_ra_get_latest_revnum(ra_session, &rev, subpool1);
	if (err) goto hit_error;
	if (rev) { printf("The latest revision is %ld.\n", rev); };
			
	const char *basename = svn_uri_basename(url, subpool1);// temporary added to subpool1

	target_dir = svn_dirent_join(wc_path, basename, subpool1);// temporary added to subpool1

	/*Do a Checkout*/
	checkout_from_repo(url, target_dir, result_rev, ctx, subpool1);
	
	/*print content*/
	//get_content_of_file(file, ra_session, pool);

	//Check if arguments given
	if (argc == 1){
		printf("No Arguments: Can not commit\n");
		system("pause");
		return EXIT_FAILURE;
	}

	/****Add Data to file*********************/
	
	char *target_f = strdup(target_dir);
	strcat(strcat(target_f, "/"), file);
	const char *target_file = strdup(target_f);

	FILE *protocol;
	protocol = fopen(target_file, "a+");

	time_t t;
	struct tm *ts;

	t = time(NULL);
	ts = localtime(&t);
		
	const char *arg = argv[1];
	
	char *stream = strdup(asctime(ts));
	strcat(stream, arg);
	strcat(stream, "\n");

	if (protocol != NULL){
		fprintf(protocol, stream);
		fclose(protocol);
	}

	/***Commit data and create commit message*************************************/
	// TODO test for changes (svn status)

	const char *message = argv[1];
	
	// int messlen = strlen(message);
	
	char *azeichen1 = "\"";
	char *azeichen2 = "\"";

	const int messlen = strlen(message) + strlen(azeichen1) + strlen(azeichen2) + 1;
	
	char *tempmessage = malloc(messlen);	
	//tempmessage[0] = '\0';
	
	if (tempmessage == NULL) return printf("No message");

	strcat(tempmessage, azeichen1);
	strcat(tempmessage, message);
	strcat(tempmessage, azeichen2);
	//tempmessage[messlen] = '\0';
	
	char *commit = strdup(commit_cmd);

	char *command = strdup(commit);
	strcat(command, tempmessage); // Message can be any char 
	printf("\n");
	printf(command);
	printf("\n");
	// Do the commit
	system(command);
	printf("\n");

	/***************************************/
	
	/*print content*/
	//get_content_of_file(file, ra_session, pool);


	svn_pool_destroy(subpool1);
	svn_pool_destroy(pool);
	//system("pause");
	return EXIT_SUCCESS;

hit_error:
	svn_handle_error2(err, stderr, FALSE, "svnlogger: ");
	return EXIT_FAILURE;
}


static svn_auth_baton_t * createAuthenticationBaton(svn_auth_baton_t *ab, const char *username,
	const char *password, const char *config_dir, apr_pool_t *pool)
{
	//apr_pool_t *subpool_baton = svn_pool_create(pool);
	svn_error_t *err;

	err = svn_cmdline_create_auth_baton(&ab,
		FALSE,
		username,
		password,
		config_dir,
		FALSE, // was NULL
		FALSE, // was NULL
		NULL,
		NULL,
		NULL,
		pool);
	if (err){
		svn_handle_error2(err, stderr, FALSE, "svnlogger: ");
	}

	//svn_pool_destroy(subpool_baton);

	return ab;
}

static svn_error_t *
get_content_of_file(const char *file, svn_ra_session_t *ra_session, apr_pool_t *pool)
{
	apr_pool_t *subpool = svn_pool_create(pool);
	svn_error_t *err;
	svn_stream_t *stream;

	/** Get the content of the file*/
	err = svn_stream_for_stdout(&stream, subpool);
	if (err){
		svn_handle_error2(err, stderr, FALSE, "svnlogger: ");
	}
	/** Show the content of protocol*/
	// TODO File not exist error Handle
	err = svn_ra_get_file(ra_session, file, SVN_INVALID_REVNUM, stream, NULL, NULL, subpool);
	if (err){
		svn_handle_error2(err, stderr, FALSE, "svnlogger: ");
	}
	svn_pool_destroy(subpool);
	return SVN_NO_ERROR;
}

static void
checkout_from_repo(const char *url, const char *target_dir, svn_revnum_t *result_rev,
svn_client_ctx_t *ctx, apr_pool_t *pool)
{

	apr_pool_t *subpool = svn_pool_create(pool);
	svn_error_t *err;
	svn_opt_revision_t peg_revision;
	svn_opt_revision_t op_rev;
	op_rev.kind = svn_opt_revision_head;
	const char *true_url;

	/*Create a working Copy (Checkout)*/
	err = svn_opt_parse_path(&peg_revision, &true_url, url, subpool);
	if (err) fprintf(stderr, err->message);

	err = svn_client_checkout3(result_rev,
		true_url,
		target_dir,
		&peg_revision,
		&op_rev,
		svn_depth_unknown,
		TRUE,
		FALSE,
		ctx, subpool);
	if (err) fprintf(stderr, err->message);

	svn_pool_destroy(subpool);
}