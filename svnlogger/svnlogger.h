
#ifndef SVNLOGGER_H
#define SVNLOGGER_H

#include "svn_client.h"
#include "svn_cmdline.h"
#include "svn_pools.h"
#include "svn_config.h"
#include "svn_fs.h"
#include "svn_ra.h"
#include "svn_path.h"
#include "svn_repos.h"

/* Display a prompt and read a one-line response into the provided buffer,
removing a trailing newline if present. */
static svn_error_t *
prompt_and_read_line(const char *prompt, char *buffer, size_t max);

/*Print the content of the File to the console*/
static svn_error_t *
get_content_of_file(const char *file, svn_ra_session_t *ra_session, apr_pool_t *pool);

/*Do a Checkout from the Repository*/
static void
checkout_from_repo(const char *url, const char *target_dir, svn_revnum_t *result_rev,
svn_client_ctx_t *ctx, apr_pool_t *pool);

/*Authentication Stuff, create the auth_baton object*/
static svn_auth_baton_t * createAuthenticationBaton(svn_auth_baton_t *ab, const char *username,
	const char *password, const char *config_dir, apr_pool_t *pool);

#endif
