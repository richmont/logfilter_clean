///
/// \file loguser.c
/// \author Richard Heininger - richmont12@gmail.com
/// loguser - user client text interface and loguser entrypoint
///


#include "loguser.h"
#include "client.h"

///
/// loguser entrypoint
///
int __cdecl main(int argc, TCHAR *argv[]) {
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);
	LU_CTX	*context;

	// http://msdn.microsoft.com/en-us/library/windows/desktop/aa366803(v=vs.85).aspx
	context = VirtualAlloc(NULL, sizeof(LU_CTX), MEM_COMMIT, PAGE_READWRITE);
	if (context == NULL) {
		DEB_PRINT("got no mem for context... \n");
		return -1;
	}

	// set context
	context->stop = FALSE;
	PCSTR file = {"logfile.log"};
	context->logfile = initLogFile(file);
	context->test = FALSE;

	// no testing on what the argument is, if something is set: do testsetup.
	if (TESTCASE) {
		DEB_PRINT("testcase entered\n");
		context->test = TRUE;
		// connect to logfilter
		connect(context);

		// create logging thread
		startLogThread(context);
		while (context->stop == FALSE) {
			Sleep(50000);
		}
		DEB_PRINT("testcase done\n");
		return EXIT_SUCCESS;
	}

	// start command mode
	PRINT("Press 'c' to enter command mode or 'q' to quit: ");
	TCHAR in;
	while ((in = (TCHAR)_gettchar()) != _T('q')) {
		// command mode
		if (in == _T('c')) {
			DWORD c;
			TCHAR line[127];
			BOOLEAN run = TRUE;

			while (run) {
				PRINT("> ");
				for (c = 0; c < 127 && ((in = (TCHAR)_gettchar()) != _T('\n')); c++) {
					line[c] = in;
				}
				line[c] = _T('\0');

				DEB_PRINT("loguser: got command: %s \n", line);

				TCHAR arg[125];
				if (line[2] == _T('-')) {
					for (int i = 0; i < 124; i++) {
						arg[i] = line[i + 3];
					}
					DEB_PRINT("entered argument: %s \n", arg);
				}

				switch (line[0]) {
				case _T('q'):
					PRINT("quitting..\n");
					run = FALSE;
					break;
				case _T('l'):
					PRINT("loading driver...\n");
					loadDriver();
					break;
				case _T('u'):
					PRINT("unloading driver...\n");
					unloadDriver();
					break;
				case _T('c'):
					PRINT("connect to driver...\n");
					connect(context);
					break;
				case _T('s'):
					PRINT("starting logger Thread...\n");
					startLogThread(context);
					break;
				case _T('k'):
					PRINT("kill logger thread... (this takes a second)\n");
					context->stop = TRUE;
					// instead synch...
					Sleep(SLEEP_TIME * 5 * 2);
					if (context->thread) {
						CloseHandle(context->thread);
					}
					break;
				case _T('a'):
					PRINT("attaching device...\n");
					addDevice(arg);
					break;
				case _T('d'):
					PRINT("dettaching device...\n");
					delDevice(arg);
					break;
				case _T('v'):
					PRINT("attached devices: \n");
					listDevices();
					break;
				default:
					PRINT("\ncommands: \n\n");
					PRINT("        l -- load logfilter \n");
					PRINT("        u -- unload logfilter\n");
					PRINT("        c -- connect to logfilter\n");
					PRINT("        s -- start logger thread\n");
					PRINT("        k -- kill logger thread\n");
					PRINT("        v -- view attached devices\n");
					PRINT("        a -devname -- attach devname to logfilter\n");
					PRINT("        d -devname -- detach devname from logfilter\n");
					PRINT("        q -- quit  \n");
					break;
				}
			}
			// quit...	
		}
		break;
	}

	// close handles..
	if (context->thread) {
		CloseHandle(context->thread);
	}
	if (context->port != INVALID_HANDLE_VALUE) {
		CloseHandle(context->port);
	}
	if (context->logfile != INVALID_HANDLE_VALUE) {
		CloseHandle(context->logfile);
	}

	// free mem
	VirtualFree(context, sizeof(LU_CTX), MEM_DECOMMIT);
}


