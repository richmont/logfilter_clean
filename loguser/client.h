///
/// \file client.h
/// \author Richard Heininger - richmont12@gmail.com
/// client helper functions header file
///
#ifndef __CLIENT_H__
#define __CLIENT_H__

#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <stdlib.h> 
#include <fltuser.h>

#include "loglib.h"
#include "loguser.h"


// defines
#define RECORD_BUFFER_SIZE			1024*100 // TODO : *sizeof(LF_ENTRY)
#define SLEEP_TIME					500
#define HRESULT_PRIVLEGE_NOT_HELD	0x80070522

#define MAX_FILENAME_LENGTH		256*sizeof(WCHAR)	///< sets the max length of the filename
#define MAX_EXTENSION_LENGTH	16*sizeof(WCHAR)	///< sets the max length of the extension



/// struct that holds data for describing one file acces action
typedef struct  {
	LARGE_INTEGER	TimeStart;			///< for time measuring, time when the fileaccess firstly hits the minifilter
	BOOLEAN			UserMode;			///< true if accessor was in usermode, false if admin				
	ULONG			IoFlags;			///< flags of the access
	UCHAR			MajorFunc;			///< majorfunction number
	UCHAR			MinorFunc;			///< minorfunction number
	UCHAR			OpFlags;			///< operation flags
	ULONG			IrpFlags;			///< irp flags
	WCHAR			FileName[MAX_FILENAME_LENGTH];			///< array that holds the filenamestring
	ULONG			FileNameLength;							///< the length of the filenamestring
	WCHAR			FileExtension[MAX_EXTENSION_LENGTH];	///< array that holds the filename extension string
	ULONG			FileExtensionLength;					///< the length of the filename extension string
	ULONG_PTR		ThreadId;								///< threadid from the creator of the fileaccess
	ULONG_PTR		ProcessId;								///< process id from the invoker of the action
	BOOLEAN			Directory;								///< true if file access is looking at a directory, false if file
	LARGE_INTEGER	FileSize;								///< only set if directory is false, then it holds the filesize
	NTSTATUS		IoStatus;								///< status of the operation
	LARGE_INTEGER	TimeEnd;								///< for time measuring, time when our miniifilter is done with processing the action
	USHORT			level;									///< for debugging
} LF_DATA, *PLF_DATA;



/// this struct is the container of one entry. it holds the necessary data to work easily with our LF_DATA struct
typedef struct {
	LIST_ENTRY		List;			///< member, thats is needed to use the doublelinkedlist in the windows kernel. for more see msdn
	LF_DATA			Record;			///< the actual Record
	ULONG			RecordNumber;	///< the unique recordnumber
	ULONG			Length;			///< the length of the entry
} LF_ENTRY, *PLF_ENTRY;




/// enum for different commands in communication with user
typedef enum {
	LF_GET_RECORDS = 0x2	///< only command is to get the records hold by the filter
} LF_COMMAND;



/// struct that holds the command enum
typedef struct {
	LF_COMMAND	command;	///< duplicate in server.h, the command
}LF_MSG, *PLF_MSG;


DWORD WINAPI loggerHandle(LPVOID data);

void listDevices();
void loadDriver();
void unloadDriver();
void addDevice(TCHAR *arg);
void delDevice(TCHAR *arg);

BOOLEAN setPriv(LPCTSTR lpszPrivilege, BOOL bEnablePrivilege);
ULONG IsAttachedToVolume(LPCWSTR VolumeName);
BOOLEAN connect(PLU_CTX context);
BOOLEAN startLogThread(PLU_CTX context);
BOOLEAN saveLogToFile(HANDLE logfile, LPCSTR logline);
HANDLE initLogFile(LPCSTR logfilename);

#define ENABLE_SE_LOAD_DRIVER_PRIVILEGE					\
if (!setPriv(SE_LOAD_DRIVER_NAME, TRUE))			\
{									\
	PRINT("Could not enable SeLoadDriverPrivilege, sure you're privileged?\n"); \
}

#ifndef Add2Ptr
#define Add2Ptr(P,I) ((PVOID)((PUCHAR)(P) + (I)))
#endif


#endif