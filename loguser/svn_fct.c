#include <stdio.h>

#include "svnlib.h"

/*================SETUP====================================================*/

static const char *REALM = "testrealm";
static const char *USERNAME = "test";
static const char *PASSWORD = "test";

// URL to connect the SVN Server:
static const char *URL = "http://hugonb2:8080/svn/teamprojekt";
//static const char *URL = "http://192.168.11.36:8080/svn/teamprojekt";
//static const char *URL = "http://192.168.2.108:8080/svn/teamprojekt";

// Client name:
static const char *CLIENTNAME = "Filtertreiber";
//Working path:
static const char *WCPATH = "C:/temp/svn/";
// File for the Record:
static const char *PROTOCOL_FILE = "protocol.txt";
// Command to do the commit:
static const char *COMMIT_CMD = "svn commit C:/temp/svn/teamprojekt/protocol.txt --message ";
//static const char *COMMIT_CMD = "E:/csvn/bin/svn.exe commit C:/temp/svn/teamprojekt/protocol.txt --message ";
//static const char *COMMIT_CMD = "D:/Programme/svn/bin/svn.exe commit C:/temp/svn/teamprojekt/protocol.txt --message ";

/*==========================================================================*/

/* Display a prompt and read a one-line response into the provided buffer,
removing a trailing newline if present. */
static svn_error_t *
prompt_and_read_line(const char *prompt, char *buffer, size_t max);

/*Print the content of the File to the console*/
static svn_error_t *
get_content_of_file(const char *file, svn_ra_session_t *ra_session, apr_pool_t *pool);

/*Do a Checkout from the Repository*/
static void
checkout_from_repo(const char *url, const char *target_dir, svn_revnum_t *result_rev,
svn_client_ctx_t *ctx, apr_pool_t *pool);

int mainfunction(const char *record)
//int main(int argc, const char* argv[])
{
	//const char *realm = REALM;
	const char *username = USERNAME;
	const char *password = PASSWORD;
	const char *url = URL;
	const char *wc_path = WCPATH;
	const char *file = PROTOCOL_FILE;

	/*Create a memory pool*/
	apr_pool_t *pool;
	//apr_pool_t *commit_pool;

	const char *config_dir = NULL;
	apr_hash_t *cfg_hash;

	svn_error_t *err;

	svn_revnum_t rev = 0;
	svn_client_ctx_t *ctx;
	svn_ra_session_t *ra_session = NULL;
	svn_auth_baton_t *ab;

	const char *target_dir;
	svn_revnum_t *result_rev = NULL;
	
	/*Setup the APR internal data structures*/
	if (svn_cmdline_init("my small svn client", NULL) != 0)
		//return EXIT_FAILURE;
		printf("Error: Init svn_cmdline_init");

	/*Create top-level pool*/
	pool = svn_pool_create(NULL);
	// initialize remote access API
	svn_ra_initialize(pool);

	/*Create Client context*/
	err = svn_client_create_context(&ctx, pool);
	if (err) goto hit_error;
	ctx->client_name = CLIENTNAME;

	/*Check if config files exist*/
	err = svn_config_ensure(config_dir, pool);
	if (err) goto hit_error;

	err = svn_config_get_config(&cfg_hash, NULL, pool);
	if (err) goto hit_error;
	ctx->config = cfg_hash;

	/*Authication Stuff, create the auth_baton object*/
	err = svn_cmdline_create_auth_baton(&ab,
		FALSE,
		username,
		password,
		config_dir,
		FALSE, // was NULL
		FALSE, // was NULL
		NULL,
		NULL,
		NULL,
		pool);
	if (err) goto hit_error;
	/*Store the auth_naton in ctx*/
	ctx->auth_baton = ab;

	svn_ra_callbacks2_t *callbacks;
	/*Create RA Callbacks*/
	err = svn_ra_create_callbacks(&callbacks, pool);
	if (err) goto hit_error;
	callbacks->auth_baton = ab;
	
	/* Validate the REPOS_URL */
	if (!svn_path_is_url(url)){
		printf("does not appear to be a URL");
	};
	//TODO error handle
	err = svn_client_open_ra_session(&ra_session, url, ctx, pool);
	if (err) goto hit_error;
	err = svn_ra_open4(&ra_session, NULL, URL, NULL, callbacks, NULL, cfg_hash, pool);

	fprintf(stdout, "connected to %s\n", url);
	err = svn_ra_get_latest_revnum(ra_session, &rev, pool);
	if (err) goto hit_error;
	if (rev) { printf("The latest revision is %ld.\n", rev); };

	target_dir = svn_dirent_join(wc_path, svn_uri_basename(url, pool), pool);

	/*Do a Checkout*/
	checkout_from_repo(url, target_dir, wc_path, result_rev, ctx, pool);
	/*print content*/
	get_content_of_file(file, ra_session, pool);


	/****Add Data to file*********************/
	char *target_f = strdup(target_dir);
	strcat(strcat(target_f, "/"), file);
	const char *target_file = strdup(target_f);

	FILE *protocol;
	protocol = fopen(target_file, "a");

	time_t t;
	struct tm *ts;

	t = time(NULL);
	ts = localtime(&t);
	//TODO Call the Program with the right arguments
	/*const char *arg = argv[1];
	int i;
	for (i = 2; i < argc; i++){
		strcat(arg, argv[i]);
	}*/

	char *stream = strdup(asctime(ts));
	//strcat(stream, arg);
	strcat(stream, record);
	strcat(stream, "\n");

	if (protocol != NULL){
		fprintf(protocol, stream);
		fclose(protocol);
	}

	/***Commit data and create commit message*************************************/
	// TODO test for changes (svn status)

	//const char *message = argv[1];
	const char *message = record;
	// int messlen = strlen(message);
	
	char *azeichen1 = "\"";
	char *azeichen2 = "\"";

	const int messlen = strlen(message) + strlen(azeichen1) + strlen(azeichen2) + 1;
	
	char *tempmessage;
	tempmessage = malloc(messlen);
	tempmessage[0] = '\0';

	strcat(tempmessage, azeichen1);
	strcat(tempmessage, message);
	strcat(tempmessage, azeichen2);
	//tempmessage[messlen] = '\0';

	char *commit = strdup(COMMIT_CMD);

	char *command = strdup(commit);
	strcat(command, tempmessage); // Message can be any char 
	printf("\n");
	printf(command);
	printf("\n");
	// Do the commit
	system(command);
	printf("\n");

	/***************************************/
		
	get_content_of_file(file, ra_session, pool);

	svn_pool_destroy(pool);

	return EXIT_SUCCESS;

hit_error:
	svn_handle_error2(err, stderr, FALSE, "my_svn_client: ");
	return EXIT_FAILURE;
}

static svn_error_t *
get_content_of_file(const char *file, svn_ra_session_t *ra_session, apr_pool_t *pool)
{
	apr_pool_t *subpool = svn_pool_create(pool);
	svn_error_t *err;
	svn_stream_t *stream;

	/** Get the content of the file*/
	err = svn_stream_for_stdout(&stream, subpool);
	if (err){
		svn_handle_error2(err, stderr, FALSE, "my_svn_client: ");
	}
	/** Show the content of protocol*/
	// TODO File not exist error Handle
	err = svn_ra_get_file(ra_session, file, SVN_INVALID_REVNUM, stream, NULL, NULL, subpool);
	if (err){
		svn_handle_error2(err, stderr, FALSE, "my_svn_client: ");
	}
	svn_pool_destroy(subpool);
	return SVN_NO_ERROR;
}

static void
checkout_from_repo(const char *url, const char *target_dir, svn_revnum_t *result_rev,
svn_client_ctx_t *ctx, apr_pool_t *pool)
{

	apr_pool_t *subpool = svn_pool_create(pool);
	svn_error_t *err;
	svn_opt_revision_t peg_revision;
	svn_opt_revision_t op_rev;
	op_rev.kind = svn_opt_revision_head;
	const char *true_url;

	/*Create a working Copy (Checkout)*/
	err = svn_opt_parse_path(&peg_revision, &true_url, url, subpool);
	if (err) fprintf(stderr, "Error svn_opt_parse_path: ", err->message);

	err = svn_client_checkout3(result_rev,
		true_url,
		target_dir,
		&peg_revision,
		&op_rev,
		svn_depth_unknown,
		TRUE,
		FALSE,
		ctx, subpool);
	if (err) fprintf(stderr, "Error Checkout", err->message);

	svn_pool_destroy(subpool);
}

static svn_error_t *
prompt_and_read_line(const char *prompt, char *buffer, size_t max)
{
	int len;
	printf("%s: ", prompt);
	if (fgets(buffer, max, stdin) == NULL)
		return svn_error_create(0, NULL, "error reading stdin");
	len = strlen(buffer);
	if (len > 0 && buffer[len - 1] == '\n')
		buffer[len - 1] = 0;
	return SVN_NO_ERROR;
}