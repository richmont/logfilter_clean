///
/// \file client.c
/// \author Richard Heininger - richmont12@gmail.com
/// loguser helper functions for using fltmgr through the user client
/// and connecting to the logfilter minifilter driver
///
#include "client.h"
#include "loguser.h"
#include "loglib.h"


///
/// Adds a device specified through *arg
///
void addDevice(TCHAR *arg) {
	HRESULT res;
	WCHAR name[INSTANCE_NAME_MAX_CHARS + 1];

	res = LLAttach(LF_SERV_NAME, (LPCWSTR)arg, NULL, sizeof(name), name);
	if (SUCCEEDED(res)) {
		PRINT("filter is now attached to %s (%s) \n", arg, name);
	}
	else {
		PRINT("failed attaching device...  (res => %#08x)\n", res);
	}
}

///
/// Deletes a device specified through arg
///
void delDevice(TCHAR *arg){
	HRESULT res;
	WCHAR name[INSTANCE_NAME_MAX_CHARS + 1];

	res = LLDetach(LF_SERV_NAME, (LPCWSTR)arg, NULL);
	if (SUCCEEDED(res)) {
		PRINT("filter is now detached from %s \n", arg, name);
	}
	else {
		PRINT("failed detaching device...  (res => %#08x)\n", res);
	}
}

///
/// unloads the minifilter
///
void unloadDriver() {
	ENABLE_SE_LOAD_DRIVER_PRIVILEGE;

	HRESULT res;
	res = FilterUnload(LF_SERV_NAME);

	if (res == S_OK) {
		PRINT("minifilter unloaded. \n");
	}
	else {
		PRINT("failed unloading driver res = %#08x \n", res);
	}
}


///
/// loads the driver
///
void loadDriver() {
	ENABLE_SE_LOAD_DRIVER_PRIVILEGE;

	HRESULT res;
	res = LLLoad(LF_SERV_NAME);
	if (res != S_OK) {
		if (res == HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS)) {
			PRINT("minifilter is already loaded. \n");
		}
		else if (res == HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND)) {
			PRINT("minifilter not found.\n");
		}
		else if (res == HRESULT_FROM_WIN32(ERROR_SERVICE_ALREADY_RUNNING)) {
			PRINT("minifilter already running.\n");
		}
		else if (res == HRESULT_FROM_WIN32(ERROR_BAD_EXE_FORMAT) || res == HRESULT_FROM_WIN32(ERROR_BAD_DRIVER)) {
			PRINT("image is corrupted.\n");
		}
		else if (res == HRESULT_FROM_WIN32(ERROR_INVALID_IMAGE_HASH)) {
			PRINT("invalid signed.\n");
		}
		else if (res == HRESULT_FROM_WIN32(ERROR_ACCESS_DENIED) || res == HRESULT_PRIVLEGE_NOT_HELD) {
			PRINT("access denied, need admin rights. \n");
		}
		else {
			PRINT("error: %#8x\n", res);
		}
	}
	else {
		PRINT("minifilter loaded. \n");
	}

}

///
/// This is a function from a similar project called proctracefs
/// It sets and cheks the needed priviliges to work with fltmgr
///
// this function is code from ptfs / user / driver.c
// http://msdn.microsoft.com/en-us/library/windows/desktop/aa446619(v=vs.85).aspx
BOOLEAN setPriv(LPCTSTR lpszPrivilege, BOOL bEnablePrivilege)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;
	HANDLE hToken;

	OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken);

	if (!LookupPrivilegeValue(
		NULL,            // lookup privilege on local system
		lpszPrivilege,   // privilege to lookup 
		&luid))          // receives LUID of privilege
	{
		PRINT("LookupPrivilegeValue error: %u\n", GetLastError());
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	if (bEnablePrivilege)
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tp.Privileges[0].Attributes = 0;

	// Enable the privilege or disable all privileges.
	if (!AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		(PTOKEN_PRIVILEGES)NULL,
		(PDWORD)NULL))
	{
		PRINT("AdjustTokenPrivileges error: %u\n", GetLastError());
		return FALSE;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

	{
		PRINT("The token does not have the specified privilege. \n");
		return FALSE;
	}

	return TRUE;
}
// also from ptfs...
///
/// code from ptfs
/// checks if a volume is attached to the driver
///
ULONG IsAttachedToVolume(LPCWSTR VolumeName)
{
	PWCHAR filtername;
	CHAR buffer[1024];
	PINSTANCE_FULL_INFORMATION data = (PINSTANCE_FULL_INFORMATION)buffer;
	HANDLE Iterator = INVALID_HANDLE_VALUE;
	ULONG bytesReturned;
	ULONG instanceCount = 0;
	HRESULT hResult;

	hResult = FilterVolumeInstanceFindFirst(VolumeName,
		InstanceFullInformation,
		data,
		sizeof(buffer)-sizeof(WCHAR),
		&bytesReturned,
		&Iterator);

	if (IS_ERROR(hResult)) {
		return instanceCount;
	}

	do {

		__analysis_assume((data->FilterNameBufferOffset + data->FilterNameLength) <= (sizeof(buffer)-sizeof(WCHAR)));

		filtername = Add2Ptr(data, data->FilterNameBufferOffset);
		filtername[data->FilterNameLength / sizeof(WCHAR)] = L'\0';

		if (_wcsicmp(filtername, LF_SERV_NAME) == 0) {
			instanceCount++;
		}

	} while (SUCCEEDED(FilterVolumeInstanceFindNext(Iterator,
		InstanceFullInformation,
		data,
		sizeof(buffer)-sizeof(WCHAR),
		&bytesReturned)));

	FilterVolumeInstanceFindClose(Iterator);

	return instanceCount;
}




///
/// lists all devices
///
void listDevices() {
	HRESULT res;
	FILTER_VOLUME_BASIC_INFORMATION *buf;
	DWORD bufSize = 1024 * 10;
	DWORD returned;
	HANDLE it;
	WCHAR name[MAX_PATH];


	buf = VirtualAlloc(NULL, bufSize, MEM_COMMIT, PAGE_READWRITE);
	if (buf == NULL) {
		DEB_PRINT("listDevices: got no mem. \n");
		return;
	}

	res = FilterVolumeFindFirst(FilterVolumeBasicInformation, buf, bufSize, &returned, &it);
	if (IS_ERROR(res)) {
		DEB_PRINT("listDevices: FilterVolumeFindFirst failed. \n");
		return;
	}

	if (it != INVALID_HANDLE_VALUE) {
		PRINT("%-14s | %-36s | %-10s\n", _T("Dos Name"), _T("Volume Name"), _T("attached?"));
		PRINT("%-14s | %-36s | %-10s\n", _T("--------"), _T("-----------"), _T("------------"));

		do {
			PRINT("%-14s | %-36s | %-10d\n",
				(SUCCEEDED(FilterGetDosName(
				buf->FilterVolumeName,
				name,
				sizeof(name) / sizeof(WCHAR))) ? name : L""),
				buf->FilterVolumeName,
				IsAttachedToVolume(buf->FilterVolumeName));
		} while (SUCCEEDED(res = FilterVolumeFindNext(it,
			FilterVolumeBasicInformation,
			buf,
			bufSize,
			&returned)));

		FilterVolumeFindClose(it);

		if (IS_ERROR(res)) {
			if (res != HRESULT_FROM_WIN32(ERROR_NO_MORE_ITEMS)) {
				if (res == HRESULT_PRIVLEGE_NOT_HELD) {
					PRINT("Access is denied, you need Administrator privileges to run this command!\n");
				}
				PRINT("Device listing failed with error: %#08x\n", res);
			}

		}
		VirtualFree(buf, bufSize, MEM_DECOMMIT);
	}





}


///
/// connect to logfilter
///
BOOLEAN connect(PLU_CTX context) {
	DEB_PRINT("connect to logfilter... \n");
	HRESULT hres = LLConnect(LF_PORT_NAME, &context->port);
	if (hres != S_OK) {
		DEB_PRINT("connecting to logfilter failed. hres= %#08x \n", hres);
		return FALSE;
	}
	return TRUE;

}


///
/// starts the loggerthread, that takes data from the miniflter
///
BOOLEAN startLogThread(PLU_CTX context) {
	DEB_PRINT("create logging thread.. \n");
	context->thread = CreateThread(NULL,
		0,
		(LPTHREAD_START_ROUTINE)loggerHandle,
		(LPVOID)context,
		0,
		&context->tid);
	if (!context->port) {
		DEB_PRINT("creating logging thread failed. getlasterror = %#08x \n", GetLastError());
		return FALSE;
	}
	return TRUE;
}


/// 
/// the tfunction that receives the data from logfilter
///
DWORD WINAPI loggerHandle(LPVOID data) {
	ULONG   packetAmount = 0;
	HRESULT hres;
	UCHAR	*buffer;
	DWORD   bufReturned = 0;
	LU_CTX	*context = (LU_CTX*)data;
	LF_MSG	msg;


	context->stop = FALSE;


	// get memory for buffer
	if ((buffer = VirtualAlloc(NULL, RECORD_BUFFER_SIZE, MEM_COMMIT, PAGE_READWRITE)) == NULL) {
		DEB_PRINT("loggerHandle: error: got no mem for records \n");
		return 0;
	}


	msg.command = LF_GET_RECORDS;	// set get command



	while (context->stop != TRUE) {
		hres = LLSend(context->port, &msg, sizeof(msg), buffer, RECORD_BUFFER_SIZE, &bufReturned);
		if (IS_ERROR(hres)) {
			DEB_PRINT("loggerHandle: FilterSendMessage error: %#x\n", hres);
			if (HRESULT_FROM_WIN32(ERROR_INVALID_HANDLE) == hres) {
				DEB_PRINT("loggerHandle: error: invalid handle \n");
			}
			else if (HRESULT_FROM_WIN32(ERROR_NO_MORE_ITEMS) == hres) {
				DEB_PRINT("loggerHandle: no more items: %#x\n", hres);
			}
			else {
				DEB_PRINT("loggerHandle: error: unexpected error: %#x\n", hres);
				break;
			}
			Sleep(SLEEP_TIME);
		}
		else {

			packetAmount++;
			PLF_ENTRY entry = (PLF_ENTRY)buffer;
			PLF_DATA data = &entry->Record;

			DEB_PRINT("got correct packet nr. %u containing record nr. %u\n", packetAmount, entry->RecordNumber);
			
			DEB_PRINT("start: %lld end: %lld timeinsystem: todo\n", data->TimeStart, data->TimeEnd);
			DEB_PRINT("major: %u minor: %u filename: %ws \n", data->MajorFunc, data->MinorFunc, data->FileName);


			if (context->test) {
				
					WIN32_FIND_DATA FindFileData;
					HANDLE handle = FindFirstFile("testisdone", &FindFileData);
					int found = handle != INVALID_HANDLE_VALUE;
					if (found)
					{
						//FindClose(&handle); this will crash
						FindClose(handle);
						context->stop = TRUE;
					}
					
				
			}


			char log[512];			
			sprintf_s(log,sizeof(char)*512 ,"%lu|%lld|%lld|%ws\n", entry->RecordNumber, data->TimeStart, data->TimeEnd,data->FileName);
			DEB_PRINT("%s",log);
			saveLogToFile(context->logfile,log);						

			if (bufReturned == 0) {
				DEB_PRINT("loggerHandle: got no items, go to sleep..\n");
				Sleep(SLEEP_TIME);
			}

		}

	}

	context->stop = TRUE;
	// sleep 5 times instead of using synchro
	Sleep(SLEEP_TIME * 5);


	DEB_PRINT("loggerHandle: releasing buffer memory... \n");
	VirtualFree(buffer, RECORD_BUFFER_SIZE, MEM_DECOMMIT);
	DEB_PRINT("loggerHandle: exiting loggerHandle... [%u packets arrived]\n", packetAmount);
	PRINT("logger Thread is done, got %u packets. \n", packetAmount);

	return 0;
}


///
/// inits the logfile used by the loggerhandle
///
HANDLE initLogFile(LPCSTR logfilename) {
	HANDLE log;

	log = CreateFileA(logfilename,
		GENERIC_WRITE,
		FILE_SHARE_READ,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (log == INVALID_HANDLE_VALUE) {
		PRINT("could not open logfile %ws \n",logfilename);
		return log;
	}
	DEB_PRINT("opened logfile %ws \n", logfilename);
	return log;



}


///
/// saves one entry to the logfile
///
BOOLEAN saveLogToFile(HANDLE logfile, LPCSTR logline) {
	DWORD bytesWritten = 0;
//	WCHAR *buffer[600];
	
	if (!WriteFile(logfile, logline, lstrlenA(logline), &bytesWritten, NULL)) {
		DEB_PRINT("could not write log.. (log: %ws) \r\n",logline);
	}


	return TRUE;
}