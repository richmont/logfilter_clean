///
/// \file loguser.h
/// \author Richard Heininger - richmont12@gmail.com
/// loguser header file
///
#ifndef __LOGUSER_H__
#define __LOGUSER_H__

#include <windows.h>
#include <stdio.h>	
#include <tchar.h>

///
/// defines
///
#define TESTCASE FALSE		// if true, then its not possible to change behaviour of loguser
#define LF_PORT_NAME		L"\\logfilterDriver"		///< duplicate in server.h, the communication portname of our filter
#define LF_SERV_NAME		L"logfilter"
#define DEB_PRINT(fmt, ...) //_ftprintf_s(stderr, _T("deb: "fmt), __VA_ARGS__)
#define PRINT(fmt, ...)		_ftprintf_s(stderr, _T(fmt), __VA_ARGS__)



/// structure that holds all the data needed for interaction with logfilter and his own behaviour
typedef struct {
	HANDLE		port;		///< handle for the connection
	DWORD		tid;		///< thread id
	HANDLE		thread;		///< our loggerhandle thread, that takes data from the minifilter
	BOOLEAN		stop;		///< boolean to tell thread to stop
	HANDLE		logfile;	///< handle to the logfile
	BOOLEAN     test;		///< debug porposes
} LU_CTX, *PLU_CTX;

#endif