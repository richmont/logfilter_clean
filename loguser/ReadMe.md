# README #

## Content ##
1. Overview
2. Configurations
3. Files
4. How to use

## 1. Overview ##

The sources of the user client.


## 2. Configurations ##

see toplevel readme

## 3. Files ##

* client.c
	functions to communicate with kernel
* client.h
	settings for the comm.
* loguser.c
	the main
* loguser.h
	main settings
* loguser.vcxproj
	VS 2013 project file
* ReadMe.md
	this readme
* svn*
	undocumented.


## 4. How to use ##

After a succesfull build and an installed logfilter you ca use the loguser to communicate with the logfilter.

You have to run it from the commandline and direct after the start you have a menu which is self explaining.