

#ifndef CLIENT_FCT_H
#define CLIENT_FCT_H

#include "svn_client.h"
#include "svn_cmdline.h"
#include "svn_pools.h"
#include "svn_config.h"
#include "svn_fs.h"
#include "svn_ra.h"
#include "svn_path.h"
#include "svn_repos.h"

int mainfunction(const char *record);

#endif
