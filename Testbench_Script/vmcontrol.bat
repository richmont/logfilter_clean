@echo off
::Umgebungsvariablen setzen f�r vmrun (PATH : C:\Program Files (x86)\VMware\VMware Workstation)

SET vmpath=d:\vm_machines\Windows7prox86\Windows7prox86.vmx
SET scriptpath=C:\scriptload.bat
SET user=Administrator
SET pw=admin

echo vmpath ist: %vmpath%

::Bsp
::vmrun -T ws start "c:\my VMs\myVM.vmx"

echo vmcontrol.bat wird ausgefuehrt. Zum starten Taste druecken...
TIMEOUT 10

::for /L %%N IN (1, 1, 5) DO echo Nummer %%N
REM (Startzahl, Schrittweite, Endzahl) Endzahl = 1 daher einamlige ausf�hrung
for /L %%N IN (1, 1, 1) DO (

echo Start VM 
vmrun -T ws start %vmpath%
cls
for /L %%M IN (1,1,100) DO ( 
echo VM Startet bitte warten %%M
ping -n 2 127.0.0.1 > NUL
cls
)

Set starttime=%time%

rem vmrun -T ws -gu %user% -gp %pw% runProgramInGuest %vmpath% -noWait %scriptpath% 

echo Shutdown VM
pause
vmrun -T ws stop %vmpath%
)


Set endtime=%time%


 
rem set starttime=23:13:45,76
rem set endtime=00:15:26,92
rem set startdate=08.09.2012
rem set enddate=09.09.2012
set startdate=%date%
set enddate=%date%
 
 
REM einstellige Uhrzeiten korrigieren
set endtimehh=%endtime:~1,1%
if �%endtimehh%� == �:� set endtime=0%endtime%
echo endtime: %endtime%
 
set starttimehh=%starttime:~1,1%
if �%starttimehh%� == �:� set starttime=0%starttime%
echo starttime: %starttime%
 
set /a timerstart=((1%starttime:~0,2%-100)*60*60)+((1%starttime:~3,2%-100)*60)+(1%starttime:~6,2%-100)
echo timerstart: %timerstart%
 
set /a daymodifier=86400*((1%enddate:~0,2%)-(1%startdate:~0,2%))
rem set daymodifier=86400
echo daymodifier: %daymodifier%
 
set /a timerstop=(((1%endtime:~0,2%-100)*60*60)+((1%endtime:~3,2%-100)*60)+(1%endtime:~6,2%-100))+%daymodifier%
echo timerstop: %timerstop%
 
rem =======================
 
set /a timeseks=(%timerstop%-%timerstart%)
echo Sekunden: %timeseks%
 
set /a timemins=((%timerstop%-%timerstart%)/60)
echo Minuten: %timemins%
 
set /a timeminsseks=((((%timerstop%-%timerstart%)/60)*60)-(%timerstop%-%timerstart%))*-1
echo Zeit: %timemins%:%timeminsseks%
 
pause