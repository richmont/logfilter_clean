@echo off

SET driverpath=C:\DriverTest\Drivers\

cd %driverpath%
rem load logfilter
rem ====================
fltmc load logfilter
fltmc attach logfilter c:
fltmc

rem ====================

rem start logger thread
rem ====================
echo Bitte im neuen Fenster Logfilter connecten und loggerThread starten 
start cmd.exe /c "C:\Win7Release\loguser.exe"
pause
rem connect filter
rem ToDo connect and start logging automatic
rem start logging

rem ====================

rem Do filesystem accesses
rem erzeugt 100mal die Datei EmptyFile und l�scht sie wieder.
rem ====================

for /L %%N IN (1, 1, 100) DO (
echo %%N
type NUL > C:\temp\EmptyFile.txt
del C:\temp\EmptyFile.txt
)
echo Im loggerThread Fenster "k" und "enter" eingeben zum beenden des loggings
pause
rem ====================


rem unload logfilter
rem ====================

fltmc detach logfilter c:
fltmc unload logfilter
fltmc 
pause
rem ====================
