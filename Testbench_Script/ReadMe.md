#Test Bench Script

1. Script als Adminstrator ausführen
2. VM mit "vmrun" controllieren
3. Vorraussetzungen zum Start


---


#####1. Script als Adminstrator ausführen auf der VM

Kopiere die loadlogfilter.bat in `C:\`

Auf der VM mit Hilfe der Aufgabenplanung eine Aufgabe erstellen.

Dazu hab ich mich an diese anleitung gehalten:

[Create Administrator Mode Shortcuts Without UAC Prompts in Windows 7 or Vista](http://www.howtogeek.com/howto/windows-vista/create-administrator-mode-shortcuts-without-uac-prompts-in-windows-vista/)

Taskname: loadlogfilter

Als Aktion "Programm starten"wählen und den Pfad für das Skript angeben : `C:\loadlogfilter.bat`


#####2. VM mit "vmrun" controllieren

Das Script "vmcontrol.bat" ist für die äußere Kontrolle zuständig.
Es nutzt das Programm/Befehl "vmrun" von VMware.

vmrun liegt im Verzeichnis `C:\Program Files (x86)\VMware\VMware Workstation`

Damit vmrun ohne Pfad Angabe aufgerufen werden kann muss die PATH Umgebungsvariable um den obigen Pfad ergänzt werden.

Erklärungen zu vmrun finden man hier 
[vmrun command](https://www.vmware.com/support/developer/vix-api/vix112_vmrun_command.pdf)
oder 
in der console `vmrun -?` eingeben

Der Befehl startet den "loadlogfilter" Tasks auf der vm:(wird aktuell nicht mehr genutzt) 
> `vmrun -T ws -gu %user% -gp %pw% runProgramInGuest %vmpath% -noWait %scriptpath% `
 
Stattdessen wird wieder die Aufgabenplanung als Auslöser für den Script start verwendet. Dazu vorher unter Aktion "Prgramm starten" auswählen und den Pfad vergeben (`C:\scriptload.bat `). Dann unter Trigger das Event Anmeldung auswählen und verzögerter Start auf 30 Sekunden. Das speichern und vm neustarten und testen.

##### 3. Vorraussetzungen zum Start

Vorm Script start muss der Treiber auf der VM Manuell installiert werden. (Rechts klick auf Inf Datei und installieren)

Damit die Scripte mit Admin rechten gestarten werden nochmal prüfen ob der Tasks in der Aufgabenplanung "Mit höchsten Privilegien ausführen" markiert ist.

