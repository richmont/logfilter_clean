# README #

## Content ##
1. Overview
2. Configurations
3. Directories
4. How to install
5. How to use

## 1. Overview ##

The whole logfilter is written in C and with Visual Studio 2013 and its using the WINDDK and the SVN libraries.
To open the project you need to open logfilter.sln in the top level directory.
There is an additional ReadMe File in everey Directory with further information.
You may need to change some paths to be able to build the solution. 


## 2. Configurations and Pathsettings ##

Here's some stuff that you may need to do in VS 2013 to be able to build the solution.

The whole configuration is only tested for the buildsettings under Win7 Debug and Win7 Release.

In the "Project Explorer" you need to right-click the "loguser" project and choose "properties":
Under "configurationproperties"->"VC++ Directories"->"IncludeDirectories" you need to set the correct paths to the loglib library, which is also brought to you by this solution.
You also need to correct the settings under "configurationproperties"->"C/C++"->"additional includes".
and also You also need to correct the settings under "configurationproperties"->"C/C++"->"linker"

You have to do the same for the svnlogger.

The paths need to be changen according to your system.


## 3. Directories ##

* /logfilter/ 
	all sources that are needed to compile the minifilter

* /logfilter Package/
	used by VS.

* /loglib/
	a wrapper library for the FilterManager Library

* /loguser/
	all sources of the user client programm.
	needs the loglib

* /svnlogger/
	all sources of the svnlogger

* /SVNSources/
	the headers and libraries from SVN used by the svnlogger

* /Testbench_Script/
	the testbench used by us to determine the speed loss with loaded logfilter


## 4. How to install ##

After a succesfull build you will need to copy the needed files
	logfilter.inf
	logfilter.sys
	logfilter.cat
	loguser.exe
to the target system. A right-click on logfilter.inf will make you able to install the minifilter.


## 5. How to use ##

After you've succesfully installed the minifilter. you can start it's work either by using fltmgr under privileged rights or using the loguser binary, which is explained in its own readme file.

