# README #

## Content ##
1. Overview
2. Configurations
3. Files

## 1. Overview ##

A wrapper library for the Windows FilterManager Library.


## 2. Configurations ##

see toplevel readme

## 3. Files ##

* fltLib.lib
	the Windows FilterManagerLibrary
* loglib.c
	the wrapper functions
* loglib.h
	the header
* loglib.vcxproj
	the VS 2013 project file
* ReadMe.Md
	this Readme