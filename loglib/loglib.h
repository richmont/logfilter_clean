///
/// \file loglib.h
/// \author Richard Heininger - richmont12@gmail.com
/// loguser - user client text interface and loguser entrypoint
///

#ifndef __LOGLIB_H__
#define __LOGLIB_H__

#include <Windows.h>
#include <fltUser.h>


HRESULT LLConnect(LPCWSTR name, HANDLE *port);
HRESULT LLSend(HANDLE hPort, LPVOID lpInBuffer, DWORD dwInBufferSize, LPVOID lpOutBuffer, DWORD dwOutBufferSize, LPDWORD lpBytesReturned);
HRESULT LLGet(HANDLE hPort, PFILTER_MESSAGE_HEADER lpMessageBuffer, DWORD dwMessageBufferSize, LPOVERLAPPED lpOverlapped);
HRESULT LLReply(HANDLE hPort, PFILTER_REPLY_HEADER lpReplyBuffer, DWORD dwReplyBufferSize);
HRESULT LLLoad(LPCWSTR lpFilterName);
HRESULT LLUnload(LPCWSTR lpFilterName);
HRESULT LLAttach(LPCWSTR lpFilterName, LPCWSTR lpVolumeName, LPCWSTR lpInstanceName, DWORD dwCreatedInstanceNameLength, LPWSTR lpCreatedInstanceName);
HRESULT LLDetach(LPCWSTR lpFilterName, LPCWSTR lpVolumeName, LPCWSTR lpInstanceName);

#endif