///
/// \file loglib.c
/// \author Richard Heininger - richmont12@gmail.com
/// loglib is wrapper for the fltlib
///


#include <Windows.h>
#include <fltUser.h>
#include "loglib.h"

///
/// wraps FilterConnectCommunicationPort
///
HRESULT LLConnect(LPCWSTR name, HANDLE *port) {
	return FilterConnectCommunicationPort(name, 0, NULL, 0, NULL, port);
}
///
/// wraps FilterSendMessage
///
HRESULT LLSend(HANDLE hPort, LPVOID lpInBuffer, DWORD dwInBufferSize, LPVOID lpOutBuffer, DWORD dwOutBufferSize, LPDWORD lpBytesReturned) {
	return FilterSendMessage(hPort, lpInBuffer, dwInBufferSize, lpOutBuffer, dwOutBufferSize, lpBytesReturned);
}

///
/// wraps FilterGetMessage
///
HRESULT LLGet(HANDLE hPort, PFILTER_MESSAGE_HEADER lpMessageBuffer, DWORD dwMessageBufferSize, LPOVERLAPPED lpOverlapped) {
	return FilterGetMessage(hPort, lpMessageBuffer, dwMessageBufferSize, lpOverlapped);
}

///
/// wraps FilterReplyMessage
///
HRESULT LLReply(HANDLE hPort, PFILTER_REPLY_HEADER lpReplyBuffer, DWORD dwReplyBufferSize) {
	return FilterReplyMessage(hPort, lpReplyBuffer, dwReplyBufferSize);
}

///
/// wraps FilterLoad
///
HRESULT LLLoad(LPCWSTR lpFilterName) {
	return FilterLoad(lpFilterName);
}
///
/// wraps FilterUnload
///
HRESULT LLUnload(LPCWSTR lpFilterName) {
	return FilterUnload(lpFilterName);
}
///
/// wraps FilterAttach
///
HRESULT LLAttach(LPCWSTR lpFilterName, LPCWSTR lpVolumeName, LPCWSTR lpInstanceName, DWORD dwCreatedInstanceNameLength, LPWSTR lpCreatedInstanceName) {
	return FilterAttach(lpFilterName, lpVolumeName, lpInstanceName, dwCreatedInstanceNameLength, lpCreatedInstanceName);
}
///
/// wraps FilterDetach
///
HRESULT LLDetach(LPCWSTR lpFilterName, LPCWSTR lpVolumeName, LPCWSTR lpInstanceName) {
	return FilterDetach(lpFilterName, lpVolumeName, lpInstanceName);
}



