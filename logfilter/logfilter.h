///
/// \file logfilter.h
/// \author Richard Heininger - richmont12@gmail.com
/// header with infos of logfilter
///

#ifndef __LOGFILTER_H__
#define __LOGFILTER_H__

#include <recorder.h>

/******************** Makros *********/
/*************************************/
//#ifdef DEBUG
//#define DEB_PRINT(fmt, ...) PRINT(fmt, __VA_ARGS__)
//#else
#define DEB_PRINT(fmt, ...)		DbgPrint(fmt, __VA_ARGS__);
//#endif




#define PT_DBG_PRINT( _dbgLevel, _string )          \
	(FlagOn(gTraceFlags, (_dbgLevel)) ? \
	DbgPrint _string : \
	((int)0))

/// struct that has all data to run logfilter
typedef struct {
	PDRIVER_OBJECT		DriverObject;	///< Pointer to the Driver Object Structure
	PFLT_FILTER			FltFilter;		///< Pointer to our FLT_FILTER object, that holds the filter data
	LF_RECORDER			Recorder;		///< Our LF_RECORDER Structure, that holds all information about the recording
	PFLT_PORT			ServerPort;		///< Pointer to our FLT_PORT structure, which holds the infos for the servercommunication with userland
	PFLT_PORT			ClientPort;		///< Pointer to our FLT_PORT structure, which holds the infos about the incoming connection got through the serverport
} LF_CTX, *PLF_CTX;


extern LF_CTX Context;



DRIVER_INITIALIZE DriverEntry;
NTSTATUS
DriverEntry(
_In_ PDRIVER_OBJECT DriverObject,
_In_ PUNICODE_STRING RegistryPath
);

NTSTATUS
logfilterUnload(
_In_ FLT_FILTER_UNLOAD_FLAGS Flags
);

FLT_PREOP_CALLBACK_STATUS
logfilterPreOperation(
_Inout_ PFLT_CALLBACK_DATA Data,
_In_ PCFLT_RELATED_OBJECTS FltObjects,
_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
);

FLT_POSTOP_CALLBACK_STATUS
logfilterPostOperation(
_Inout_ PFLT_CALLBACK_DATA Data,
_In_ PCFLT_RELATED_OBJECTS FltObjects,
_In_opt_ PVOID CompletionContext,
_In_ FLT_POST_OPERATION_FLAGS Flags
);

NTSTATUS
logfilterInstanceQueryTeardown(
_In_ PCFLT_RELATED_OBJECTS FltObjects,
_In_ FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
);




#endif