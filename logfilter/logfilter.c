///
/// \file logfilter.c
/// \author Richard Heininger - richmont12@gmail.com
/// This is the main module of the logfilter miniFilter driver.
///



#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>

#include <logfilter.h>
#include <server.h>

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")


ULONG_PTR OperationStatusCtx = 1;

#define PTDBG_TRACE_ROUTINES            0x00000001
#define PTDBG_TRACE_OPERATION_STATUS    0x00000002

ULONG gTraceFlags = 0;

LF_CTX Context;






#ifdef ALLOC_PRAGMA
#pragma alloc_text(INIT, DriverEntry)
#pragma alloc_text(PAGE, logfilterUnload)
#pragma alloc_text(PAGE, logfilterInstanceQueryTeardown)
#endif

///
///  this defines what (IRP_MJ_CREATE) we want and who will deal it (pre & post operations 
///
CONST FLT_OPERATION_REGISTRATION Callbacks[] = {
	{ IRP_MJ_CREATE,
	0,
	logfilterPreOperation,
	logfilterPostOperation },
	{ IRP_MJ_OPERATION_END }
};


const FLT_CONTEXT_REGISTRATION Contexts[] = {
	{ FLT_CONTEXT_END }
};

///
///  This defines what we want to filter with FltMgr
///  and what our filter does
///
CONST FLT_REGISTRATION FilterRegistration = {

	sizeof(FLT_REGISTRATION),         //  Size
	FLT_REGISTRATION_VERSION,           //  Version
	0,                                  //  Flags

	Contexts,                           //  Context
	Callbacks,                          //  Operation callbacks

	logfilterUnload,					//  MiniFilterUnload

	NULL,								//  InstanceSetup
	logfilterInstanceQueryTeardown,		//  InstanceQueryTeardown
	NULL,								//  InstanceTeardownStart
	NULL,						        //  InstanceTeardownComplete

	NULL,                               //  GenerateFileName
	NULL,                               //  GenerateDestinationFileName
	NULL                                //  NormalizeNameComponent

};




///
/// \fn DriverEntry(PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
/// \param DriverObject The driver object that was created when the minifilter driver was loaded.
/// \param unused..RegistryPath Pointer to a counted Unicode string that contains a path to the minifilter driver's registry key.
///
NTSTATUS
DriverEntry(
_In_ PDRIVER_OBJECT DriverObject,
_In_ PUNICODE_STRING RegistryPath
)
{
	NTSTATUS status;

	UNREFERENCED_PARAMETER(RegistryPath);

	DEB_PRINT("DriverEntry: entered \n");

	// init recorder

	DEB_PRINT("DriverEntry: initialising recorder.. \n");
	status = InitRecorder();
	if (!NT_SUCCESS(status)) {
		DEB_PRINT("DriverEntry: init recorder failed. status=%#08x \n", status);
		return status;
	}

	// saving pointer to driver object in context
	Context.DriverObject = DriverObject;

	// register filter
	DEB_PRINT("DriverEntry: register filter... \n");
	status = FltRegisterFilter(DriverObject, &FilterRegistration, &Context.FltFilter);
	if (!NT_SUCCESS(status)) {
		DEB_PRINT("DriverEntry: registration failed. status=%#08x \n", status);
		return status;
	}

	// start data server
	InitServer();

	// start filtering
	DEB_PRINT("DriverEntry: start filtering... \n");
	status = FltStartFiltering(Context.FltFilter);
	if (!NT_SUCCESS(status)) {
		DEB_PRINT("DriverEntry: start filtering failed. status=%#08x \n", status);
		FltUnregisterFilter(Context.FltFilter);
		return status;
	}

	DEB_PRINT("DriverEntry: left");

	return status;
}

///
/// The filter manager calls this routine to notify the minifilter driver that the filter manager is about to unload the minifilter driver.
/// \fn logfilterUnload(FLT_FILTER_UNLOAD_FLAGS Flags)
/// \param Flags Bitmask of flags describing the unload request.
///
NTSTATUS
logfilterUnload(
_In_ FLT_FILTER_UNLOAD_FLAGS Flags
)
{
	UNREFERENCED_PARAMETER(Flags);

	PAGED_CODE();

	DEB_PRINT("logfilterUnload: closing serverport... \n");
	if (Context.ServerPort != NULL) {
		FltCloseCommunicationPort(Context.ServerPort);
	}
	// TODO close possible connections ? <- not necessary?

	DEB_PRINT("logfilterUnload: unregister filter... \n");
	FltUnregisterFilter(Context.FltFilter);

	DEB_PRINT("logfilterUnload: releasing recorder... \n");
	ReleaseRecorder();


	return STATUS_SUCCESS;
}

///
/// The filter manager calls this routine to allow the minifilter driver to respond to a manual detach request.
/// Such a request is received when a user-mode application calls FilterDetach or a kernel-mode component calls FltDetachVolume.
/// \fn logfilterInstanceQueryTeardown(PCFLT_RELATED_OBJECTS FltObjects, FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags)
/// \param FltObjects Pointer to an FLT_RELATED_OBJECTS structure that contains opaque pointers for the objects related to the current operation.
/// \param Bitmask of flags that describe why the given instance query teardown callback routine was called. No flags are currently defined.
///
NTSTATUS
logfilterInstanceQueryTeardown(
_In_ PCFLT_RELATED_OBJECTS FltObjects,
_In_ FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
)
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);

	PAGED_CODE();

	return STATUS_SUCCESS;
}


///
/// Pre-operation processing for I/O operations.
/// The pre-operation callback routine processes one or more types of I/O operations. This callback routine is similar to a dispatch routine in the legacy filter model.
/// \fn logfilterPreOperation(PFLT_CALLBACK_DATA Data, PCFLT_RELATED_OBJECTS FltObjects, PVOID *CompletionContext)
/// \param Data A pointer to the callback data (FLT_CALLBACK_DATA) structure for the I/O operation.
/// \param FltObjects A pointer to an FLT_RELATED_OBJECTS structure that contains opaque pointers for the objects related to the current I/O request.
/// \param CompletionContext If this callback routine returns FLT_PREOP_SUCCESS_WITH_CALLBACK or FLT_PREOP_SYNCHRONIZE,
///        this parameter is an optional context pointer to be passed to the corresponding post-operation callback routine. Otherwise, it must be NULL.
///
FLT_PREOP_CALLBACK_STATUS
logfilterPreOperation(
_Inout_ PFLT_CALLBACK_DATA Data,
_In_ PCFLT_RELATED_OBJECTS FltObjects,
_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
	FLT_PREOP_CALLBACK_STATUS status = FLT_PREOP_SUCCESS_NO_CALLBACK;

	UNREFERENCED_PARAMETER(Data);
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(CompletionContext);

	DEB_PRINT("logfilterPreOperation: entered \n");



	if (FltObjects->FileObject == NULL) {
		DEB_PRINT("PreOperation: No file given... FileObject is NULL \n");
		return status;
	}


	// first contact with the fileaction is here

	// getting mem for the record
	PLF_ENTRY entry = CreateRecord();

	if (entry != NULL) {

		entry->Record.level = 13;
		// take data
		KeQuerySystemTime(&entry->Record.TimeStart);

		// process infos
		entry->Record.ProcessId = (ULONG_PTR)PsGetCurrentProcessId;
		entry->Record.ThreadId = (ULONG_PTR)PsGetCurrentThreadId;

		// FLT_IO_PARAMETER_BLOCK : Data->Iopb
		// http://msdn.microsoft.com/en-us/library/windows/hardware/ff544638(v=vs.85).aspx
		entry->Record.UserMode = (Data->RequestorMode == UserMode) ? TRUE : FALSE;
		entry->Record.IoFlags = Data->Flags;
		entry->Record.MajorFunc = Data->Iopb->MajorFunction;
		entry->Record.MinorFunc = Data->Iopb->MinorFunction;
		entry->Record.OpFlags = Data->Iopb->OperationFlags;
		entry->Record.IrpFlags = Data->Iopb->IrpFlags;




		// getting file name and extension...
		// http://msdn.microsoft.com/en-us/library/windows/hardware/ff544633(v=vs.85).aspx
		PFLT_FILE_NAME_INFORMATION fileInfo = NULL;
		// http://msdn.microsoft.com/en-us/library/windows/hardware/ff543032(v=vs.85).aspx
		if (NT_SUCCESS(status = FltGetFileNameInformation(Data, FLT_FILE_NAME_NORMALIZED | FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP, &fileInfo))) {
			FltParseFileNameInformation(fileInfo);
			ULONG copySize;
			if (fileInfo->Name.Buffer != NULL) {
				copySize = fileInfo->Name.Length + sizeof(UNICODE_NULL);
				if (copySize > MAX_FILENAME_LENGTH) {
					copySize = MAX_FILENAME_LENGTH - sizeof(UNICODE_NULL);
				}
				RtlCopyMemory(entry->Record.FileName, fileInfo->Name.Buffer, copySize);
				entry->Record.FileNameLength = copySize;
				entry->Record.level = 2;
			}
			if (fileInfo->Extension.Buffer != NULL) {
				copySize = fileInfo->Extension.Length + sizeof(UNICODE_NULL);
				if (copySize > MAX_EXTENSION_LENGTH) {
					copySize = MAX_EXTENSION_LENGTH - sizeof(UNICODE_NULL);
				}
				RtlCopyMemory(entry->Record.FileExtension, fileInfo->Extension.Buffer, copySize);
				entry->Record.FileExtensionLength = copySize;
				entry->Record.level = 3;
			}
		}

		if (fileInfo != NULL) {

			FltReleaseFileNameInformation(fileInfo);
		}

		// mj shutdown ? -> no post op
		if (Data->Iopb->MajorFunction == IRP_MJ_SHUTDOWN) {
			logfilterPostOperation(Data, FltObjects, entry, 0);
			status = FLT_PREOP_SUCCESS_NO_CALLBACK;
		}
		else {
			*CompletionContext = entry;
			status = FLT_PREOP_SUCCESS_WITH_CALLBACK;
		}

	}

	return status;
}



///
/// Completion processing for I/O operations.
/// The post-operation callback routine performs completion processing for one or more types of I/O operations.
/// \fn logfilterPostOperation(PFLT_CALLBACK_DATA Data, PCFLT_RELATED_OBJECTS FltObjects, PVOID CompletionContext, FLT_POST_OPERATION_FLAGS Flags)
/// \param Data A pointer to the callback data (FLT_CALLBACK_DATA) structure for the I/O operation.
/// \param FltObjects A pointer to a filter manager maintained FLT_RELATED_OBJECTS structure that contains opaque pointers for the objects related to the current I/O request.
/// \param CompletionContext A context pointer that was returned by the minifilter driver's pre-operation callback (PFLT_PRE_OPERATION_CALLBACK) routine.
///        The CompletionContext pointer provides a way to communicate information from the pre-operation callback routine to the post-operation callback routine.
/// \param Flags A bitmask of flags that specifies how the post-operation callback is to be performed.
///
FLT_POSTOP_CALLBACK_STATUS
logfilterPostOperation(
_Inout_ PFLT_CALLBACK_DATA Data,
_In_ PCFLT_RELATED_OBJECTS FltObjects,
_In_opt_ PVOID CompletionContext,
_In_ FLT_POST_OPERATION_FLAGS Flags
)
{
	UNREFERENCED_PARAMETER(Data);
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(CompletionContext);
	UNREFERENCED_PARAMETER(Flags);

	DEB_PRINT("PostOperation: entered\n");

	NTSTATUS status;

	PLF_ENTRY entry = (PLF_ENTRY)CompletionContext;
	if (entry != NULL) {

		entry->Record.level = 6;
		// http://msdn.microsoft.com/en-us/library/windows/hardware/ff551107(v=vs.85).aspx
		if (FlagOn(Flags, FLTFL_POST_OPERATION_DRAINING)) {
			DeleteRecord(entry);
			return FLT_POSTOP_FINISHED_PROCESSING;
		}

		// logging stuff

		if (FltObjects->FileObject != NULL && IoGetTopLevelIrp() == NULL) {
			// http://msdn.microsoft.com/en-us/library/windows/hardware/ff545743(v=vs.85).aspx
			ULONG fileInfoLength = sizeof(FILE_ALL_INFORMATION)+256 * sizeof(WCHAR);

			PFILE_ALL_INFORMATION fileInfo;
			fileInfo = ExAllocatePoolWithTag(NonPagedPool, fileInfoLength, LF_TAG);

			RtlZeroMemory(fileInfo, fileInfoLength);
			if (fileInfo != NULL) {

				status = FltQueryInformationFile(FltObjects->Instance, FltObjects->FileObject, fileInfo, fileInfoLength, FileAllInformation, &fileInfoLength);
				if (NT_SUCCESS(status)) {
					entry->Record.Directory = fileInfo->StandardInformation.Directory;
					entry->Record.FileSize = fileInfo->StandardInformation.AllocationSize;
				}
				ExFreePoolWithTag(fileInfo, LF_TAG);
				entry->Record.level = 7;
			}
		}


		entry->Record.IoStatus = Data->IoStatus.Status;

		KeQuerySystemTime(&entry->Record.TimeEnd);
		entry->Record.level = 8;

		// TODO here we could decide if we want directories or special files

		// adding data to output buffer		
		KIRQL oldirql;
		KeAcquireSpinLock(&Context.Recorder.lock, &oldirql);
		InsertTailList(&Context.Recorder.buffer, &entry->List);
		KeReleaseSpinLock(&Context.Recorder.lock, oldirql);



	}


	return FLT_POSTOP_FINISHED_PROCESSING;
}



