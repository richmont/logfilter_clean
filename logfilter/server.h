///
/// \file server.h
/// \author Richard Heininger - richmont12@gmail.com
/// this is the header of the servercommunication helper funcs for logfilter
///
#ifndef __SERVER_H__
#define __SERVER_H__


/// defines
#define LF_PORT_NAME L"\\logfilterDriver"		///< duplicate in loguser.h, the communication portname of our filter


/// enum for different commands in communication with user
typedef enum {
	LF_GET_RECORDS = 0x2	///< only command is to get the records hold by the filter
} LF_COMMAND;


/// struct that holds the command enum
typedef struct {			
	LF_COMMAND	command;	///< duplicate in client.h, the command
}LF_MSG, *PLF_MSG;


NTSTATUS ConnectCallback
(
IN PFLT_PORT ClientPort,
IN PVOID ServerPortCookie,
IN PVOID ConnectionContext,
IN ULONG SizeOfContext,
OUT PVOID *ConnectionPortCookie
);

VOID DisconnectCallback(
	IN PVOID ConnectionCookie
	);



NTSTATUS MessageCallback(
	IN PVOID PortCookie,
	IN PVOID InputBuffer OPTIONAL,
	IN ULONG InputBufferLength,
	OUT PVOID OutputBuffer OPTIONAL,
	IN ULONG OutputBufferLength,
	OUT PULONG ReturnOutputBufferLength
	);

BOOLEAN InitServer();


#endif