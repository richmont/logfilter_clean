# README #

## Content ##
1. Overview
2. Configurations
3. Files

## 1. Overview ##

Here are the sources of the logfilter kernel core part and their project files for VS 2013.


## 2. Configurations ##

see toplevel readme

## 3. Files ##

Here we give you an small overview of the file contents. for more information use the doxygen documentation.

* logfilter.c
	minifilter core source
* logfilter.h
	minifilter core header and settings
* logfilter.inf
	minifilter information file - used to install the minifilter
* logfilter.vcxproj
	the VS 2013 project file
* ReadMe.md
	this readme
* recorder.c
	the functions used to hold and store the logging informations.
* recorder.h
	recorder settings and protos
* server.c
	functions used to communicate with the userland
* server.h
	server settings and protos

