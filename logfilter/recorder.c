///
/// \file loguser.c
/// \author Richard Heininger - richmont12@gmail.com
/// This module takes and holds the data we want to record.
/// using DoubleLinkedList: http://msdn.microsoft.com/en-us/library/windows/hardware/ff563802(v=vs.85).aspx
///


#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>

#include <logfilter.h>
#include <recorder.h>

///
/// Routine that initialises the Recorder structure
///
NTSTATUS InitRecorder() {
	NTSTATUS status = STATUS_SUCCESS;


	Context.Recorder.RecordAmount = 0;
	Context.Recorder.overallNumber = 0;
	Context.Recorder.MaxRecordAmount = MAX_RECORD_AMOUNT;

	// error handling ?
	InitializeListHead(&Context.Recorder.buffer);

	// init spinlock
	KeInitializeSpinLock(&Context.Recorder.lock);

	// is using npaged lookaside old? should we change to lookasidelist?	
	ExInitializeNPagedLookasideList(&Context.Recorder.FreeList,
		NULL,
		NULL,
		0,
		MAX_RECORD_SIZE,
		LF_TAG,							// have to be 'X' not "X" in define
		0);

	return status;
}

///
/// Routine that creates and returns an empty record
///
PLF_ENTRY CreateRecord() {
	PVOID mem;
	PLF_ENTRY entry;

	if (Context.Recorder.RecordAmount < Context.Recorder.MaxRecordAmount) {
		InterlockedIncrement(&Context.Recorder.RecordAmount);
		mem = ExAllocateFromNPagedLookasideList(&Context.Recorder.FreeList);
		if (mem == NULL) {
			InterlockedDecrement(&Context.Recorder.RecordAmount);
			// out of mem
			DEB_PRINT("CreateRecord: got no memory \n");
			return NULL;
		}
	}
	else {
		// no more records allowed
		DEB_PRINT("CreateRecord: reached maximum record amount %d \n", Context.Recorder.MaxRecordAmount);
		return NULL;
	}

	entry = mem;

	entry->Length = sizeof(LF_ENTRY);
	entry->RecordNumber = InterlockedIncrement(&Context.Recorder.overallNumber);
	// todo zeromemory

	return entry;
}

///
/// Routine that deletes given record and frees allocated memory
///
NTSTATUS DeleteRecord(PLF_ENTRY record) {
	NTSTATUS status = STATUS_SUCCESS;

	InterlockedDecrement(&Context.Recorder.RecordAmount);
	ExFreeToNPagedLookasideList(&Context.Recorder.FreeList, (PVOID)record);

	return status;
}


///
/// releases all memory used by the recorder
///
NTSTATUS ReleaseRecorder() {
	NTSTATUS status = STATUS_SUCCESS;


	PLIST_ENTRY list;
	PLF_ENTRY	entry;
	KIRQL		oldirql;

	KeAcquireSpinLock(&Context.Recorder.lock, &oldirql);

	while (!IsListEmpty(&Context.Recorder.buffer)) {
		list = RemoveHeadList(&Context.Recorder.buffer);
		KeReleaseSpinLock(&Context.Recorder.lock, oldirql);

		entry = CONTAINING_RECORD(list, LF_ENTRY, List);

		DeleteRecord(entry);

		KeAcquireSpinLock(&Context.Recorder.lock, &oldirql);
	}

	KeReleaseSpinLock(&Context.Recorder.lock, oldirql);

	ExDeleteNPagedLookasideList(&Context.Recorder.FreeList);

	return status;
}



/// 
/// copys records to the buffer pointet to by outputbuffer
///
NTSTATUS getRecords(PVOID OutputBuffer, ULONG OutputBufferLength, PULONG ReturnOutputBufferLength) {
	NTSTATUS status = STATUS_SUCCESS;
	KIRQL oirql;
	ULONG bytewritten = 0;


	KeAcquireSpinLock(&Context.Recorder.lock, &oirql);


	// this version sends only 1 logentry back... 
	if (!IsListEmpty(&Context.Recorder.buffer) && OutputBufferLength >= sizeof(LF_ENTRY)) {
		PLIST_ENTRY list = RemoveHeadList(&Context.Recorder.buffer);
		PLF_ENTRY entry = NULL;
		entry = CONTAINING_RECORD(list, LF_ENTRY, List);

		if (entry == NULL) {
			DEB_PRINT("d�d�m\n");
			goto bla;
		}

		// no space left, put it back
		if (OutputBufferLength < entry->Length) {
			InsertHeadList(&Context.Recorder.buffer, list);
			goto bla;
		}

		KeReleaseSpinLock(&Context.Recorder.lock, oirql);

		try {
			RtlCopyMemory(OutputBuffer, entry, entry->Length);
		} except(EXCEPTION_EXECUTE_HANDLER) {
			KeAcquireSpinLock(&Context.Recorder.lock, &oirql);
			InsertHeadList(&Context.Recorder.buffer, list);
			KeReleaseSpinLock(&Context.Recorder.lock, oirql);
			return GetExceptionCode();
		}

		bytewritten += entry->Length;
		OutputBufferLength -= entry->Length;

		DeleteRecord(entry);

		KeAcquireSpinLock(&Context.Recorder.lock, &oirql);
	}
bla:


	KeReleaseSpinLock(&Context.Recorder.lock, oirql);

	if (bytewritten == 0) {
		status = STATUS_NO_MORE_ENTRIES;
		//status = STATUS_BUFFER_TOO_SMALL;
	}
	else if (bytewritten > 0) {
		status = STATUS_SUCCESS;
	}

	*ReturnOutputBufferLength = bytewritten;
	return status;
}