///
/// \file server.c
/// \author Richard Heininger - richmont12@gmail.com
/// loguser - user client text interface and loguser entrypoint
///

#include <logfilter.h>
#include <fltKernel.h>
//#include <recorder.h>
#include <server.h>
#include <tchar.h>


///
/// initializes the serverport to make connection to user possible
///
BOOLEAN InitServer() {
	NTSTATUS status;
	UNICODE_STRING			port;
	PSECURITY_DESCRIPTOR	desc;
	OBJECT_ATTRIBUTES		attr;




	DEB_PRINT("InitServer: building security descriptor... \n");
	// http://msdn.microsoft.com/en-us/library/windows/hardware/ff541778(v=vs.85).aspx
	status = FltBuildDefaultSecurityDescriptor(&desc, FLT_PORT_ALL_ACCESS);

	if (!(NT_SUCCESS(status))) {
		DEB_PRINT("InitServer: building security descriptor failed. status=%#08x \n", status);
		return FALSE;
	}

	RtlInitUnicodeString(&port, LF_PORT_NAME);



	InitializeObjectAttributes(&attr,
		&port,
		OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE,
		NULL,
		desc);

	DEB_PRINT("InitServer: creating communication port...");
	// http://msdn.microsoft.com/en-us/library/windows/hardware/ff541931(v=vs.85).aspx	
	status = FltCreateCommunicationPort(Context.FltFilter,
		&Context.ServerPort,
		&attr,
		NULL,
		ConnectCallback,
		DisconnectCallback,
		MessageCallback,
		1);


	FltFreeSecurityDescriptor(desc);


	if (!(NT_SUCCESS(status))) {
		DEB_PRINT("InitServer: creating comm port failed. status=%#08x \n", status);
		return FALSE;
	}

	return TRUE;
}


///
/// is called when someone is connecting to the minifilter
///
NTSTATUS ConnectCallback(IN PFLT_PORT ClientPort, IN PVOID ServerPortCookie, IN PVOID ConnectionContext, IN ULONG SizeOfContext, OUT PVOID *ConnectionPortCookie) {
	PAGED_CODE();

	UNREFERENCED_PARAMETER(ServerPortCookie);
	UNREFERENCED_PARAMETER(ConnectionContext);
	UNREFERENCED_PARAMETER(SizeOfContext);
	UNREFERENCED_PARAMETER(ConnectionPortCookie);


	DEB_PRINT("ConnectCallback: connected, clientport: %p \n", ClientPort);

	if (Context.ClientPort == NULL)
		Context.ClientPort = ClientPort;


	return STATUS_SUCCESS;
}

///
/// is called when someones cloes the connection to the logfilter
///
VOID DisconnectCallback(IN PVOID ConnectionCookie)
{
	PAGED_CODE();

	UNREFERENCED_PARAMETER(ConnectionCookie);

	DEB_PRINT("DisconnectCallback: disconnecting... \n");

	if (Context.ClientPort != NULL)
		FltCloseClientPort(Context.FltFilter, &Context.ClientPort);

	return;
}


///
/// is called after a connection is established and when messages arrive through the connection
///
NTSTATUS MessageCallback(PVOID PortCookie, PVOID InputBuffer, ULONG InputBufferLength, PVOID OutputBuffer, ULONG OutputBufferLength, PULONG ReturnOutputBufferLength) {
	UNREFERENCED_PARAMETER(PortCookie);
	PAGED_CODE();


	TCHAR comm;
	NTSTATUS status = STATUS_SUCCESS;

	DEB_PRINT("MessageCallback: entered \n");


	if (InputBuffer != NULL && InputBufferLength >= FIELD_OFFSET(LF_MSG, command) + sizeof(TCHAR)) {


		try {
			comm = ((LF_MSG*)InputBuffer)->command;
		} except(EXCEPTION_EXECUTE_HANDLER) {
			return GetExceptionCode();
		}




		if (comm == LF_GET_RECORDS) {
			if (OutputBuffer != NULL || OutputBufferLength != 0) {
				if (IS_ALIGNED(OutputBuffer, sizeof(PVOID))) {

					status = getRecords(OutputBuffer, OutputBufferLength, ReturnOutputBufferLength);
				}
				else {
					status = STATUS_DATATYPE_MISALIGNMENT;
				}
			}
			else {
				status = STATUS_INVALID_PARAMETER;
			}
		}
		else {
			status = STATUS_INVALID_PARAMETER;
		}
	}


	return status;

}