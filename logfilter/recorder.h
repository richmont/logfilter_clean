///
/// \file recorder.h
/// \author Richard Heininger - richmont12@gmail.com
/// the header fo the recorder helper funcs for logfilter
///
#ifndef __RECORDER_H__
#define __RECORDER_H__

#include <fltkernel.h>
#include <logfilter.h>

/// versions
#define OS_VERSON_WIN8     (NTDDI_VERSION >= NTDDI_WIN8)
#define OS_VERSON_WIN7     (NTDDI_VERSION >= NTDDI_WIN7)
#define OS_VERSON_VISTA    (NTDDI_VERSION >= NTDDI_VISTA)
#define OS_VERSON_NOT_W2K  (OSVER(NTDDI_VERSION) > NTDDI_WIN2K)



/// defines
#define MAX_RECORD_AMOUNT		500					///< maximim amount of records that will be hold in the memory
#define MAX_RECORD_SIZE			sizeof(LF_ENTRY)
#define LF_TAG					'LF'
#define MAX_FILENAME_LENGTH		256*sizeof(WCHAR)	///< sets the max length of the filename
#define MAX_EXTENSION_LENGTH	16*sizeof(WCHAR)	///< sets the max length of the extension


/// struct that holds data for describing one file acces action
typedef struct  {
	LARGE_INTEGER	TimeStart;			///< for time measuring, time when the fileaccess firstly hits the minifilter
	BOOLEAN			UserMode;			///< true if accessor was in usermode, false if admin				
	ULONG			IoFlags;			///< flags of the access
	UCHAR			MajorFunc;			///< majorfunction number
	UCHAR			MinorFunc;			///< minorfunction number
	UCHAR			OpFlags;			///< operation flags
	ULONG			IrpFlags;			///< irp flags
	WCHAR			FileName[MAX_FILENAME_LENGTH];			///< array that holds the filenamestring
	ULONG			FileNameLength;							///< the length of the filenamestring
	WCHAR			FileExtension[MAX_EXTENSION_LENGTH];	///< array that holds the filename extension string
	ULONG			FileExtensionLength;					///< the length of the filename extension string
	ULONG_PTR		ThreadId;								///< threadid from the creator of the fileaccess
	ULONG_PTR		ProcessId;								///< process id from the invoker of the action
	BOOLEAN			Directory;								///< true if file access is looking at a directory, false if file
	LARGE_INTEGER	FileSize;								///< only set if directory is false, then it holds the filesize
	NTSTATUS		IoStatus;								///< status of the operation
	LARGE_INTEGER	TimeEnd;								///< for time measuring, time when our miniifilter is done with processing the action
	USHORT			level;									///< for debugging
} LF_DATA, *PLF_DATA;


/// this struct is the container of one entry.  it holds the necessary data to work easily with our LF_DATA struct
typedef struct {
	LIST_ENTRY		List;			///< member, thats is needed to use the doublelinkedlist in the windows kernel. for more see msdn
	LF_DATA			Record;			///< the actual Record
	ULONG			RecordNumber;	///< the unique recordnumber
	ULONG			Length;			///< the length of the entry
} LF_ENTRY, *PLF_ENTRY;

/// struct that contains all infos about the recorder
typedef struct {
	__volatile	LONG		RecordAmount;		///< how many records do we hold?
	LONG					MaxRecordAmount;	///< how many records will we hold at maximum at one time
	NPAGED_LOOKASIDE_LIST	FreeList;			///< used by kernel memory managment - see msdn
	__volatile LONG			overallNumber;		///< how many records have we recorderd since start?
	KSPIN_LOCK				lock;				///< lock used to access the data
	LIST_ENTRY				buffer;				///< used by kernel memory managment - see msdn
} LF_RECORDER, *PLF_RECORDER;




NTSTATUS		InitRecorder();
PLF_ENTRY		CreateRecord();
NTSTATUS		DeleteRecord(PLF_ENTRY record);
NTSTATUS        ReleaseRecorder();
NTSTATUS		getRecords(PVOID OutputBuffer, ULONG OutputBufferLength, PULONG ReturnOutputBufferLength);

#endif